/* (c) https://github.com/MontiCore/monticore */
/**
 * 
 */
package montisim;

import org.junit.Test;

/**
 *
 */
public class CLITest {

	@Test
	public void testCorrectGeneration() {
		String[] input = {"-f", "example.composition", "-p", "src/test/resources", "-t", "target/generated-sources"};
		CLI.main(input);
	}
	
	@Test
	public void testIncorrectParameter1() {
		String[] input = {"-f", "example.composition", "-p", "src/test/resources"};
		CLI.main(input);
	}
	
	@Test
	public void testIncorrectParameter2() {
		String[] input = {"-f", "example.composition"};
		CLI.main(input);
	}
	
	@Test
	public void testIncorrectParameter3() {
		String[] input = {};
		CLI.main(input);
	}
}
