/* (c) https://github.com/MontiCore/monticore */
/**
 * 
 */
package montisim;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import montisim.SimGenerator;

public class CLI {

    private static final Logger LOGGER = LogManager.getLogger(CLI.class);

    public static void main(String[] args) {

        boolean correctInput = true;

        // Collection of all possible options
        Options options = new Options();

        Option file = Option.builder("f")
                .hasArg()
                .numberOfArgs(1)
                .desc("Name of the EMA file which desribes the simulation setup")
                .build();

        Option path = Option.builder("p")
                .hasArg()
                .numberOfArgs(1)
                .desc("Path to resolve")
                .build();

        Option source = Option.builder("t")
                .hasArg()
                .numberOfArgs(1)
                .desc("Path to generation target")
                .build();

        options.addOption(file);
        options.addOption(path);
        options.addOption(source);

        // Print helper message
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("Co-Simulation - Command-Line Interface", options);

        // Parse input
        CommandLineParser commandLineParser = new DefaultParser();
        CommandLine commandLine = null;

        try {
            commandLine = commandLineParser.parse(options, args);
        } catch (ParseException e) {
            LOGGER.fatal("Could not parse command line arguments!", e);
        }

        if (commandLine == null) {
            LOGGER.fatal("Could not parse command line arguments!");
            correctInput = false;
        }

        if (!commandLine.hasOption('f')) {
            LOGGER.fatal("Invalid CLI arguments. Argument -f is missing.");
            correctInput = false;
        }
        if (!commandLine.hasOption('p')) {
            LOGGER.fatal("Invalid CLI arguments. Argument -p is missing.");
            correctInput = false;
        }
        if (!commandLine.hasOption('t')) {
            LOGGER.fatal("Invalid CLI arguments. Argument -t is missing.");
            correctInput = false;
        }

        if (correctInput) {
            String emaFileName = commandLine.getOptionValue('f');
            String resolvePath = commandLine.getOptionValue('p');
            String generationTargetPath = commandLine.getOptionValue('t');
            SimGenerator.generate(generationTargetPath, generationTargetPath, resolvePath, emaFileName);
        }
    }
}
