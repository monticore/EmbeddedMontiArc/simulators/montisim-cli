<!-- (c) https://github.com/MontiCore/monticore -->
# Command-Line Interface for MontiSim
[![pipeline status](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/montisim-cli/badges/master/pipeline.svg)](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/montisim-cli/commits/master)

The Co-Simulation Command-Line Interface(CLI) is one of two possibilities to use the [Co-Simulation Generator](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/co-simulation-gen).
The other way is desribed in the [Co-Simulation Maven Plugin](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/maven-cosimulation-gen-plugin).

The CLI allows to evoke the generation process manually. In order to do this, there are three parameters which have to be passed to the CLI:

* -p: Path to resolve when parsing EMA files.
* -f: Path to the EMA descriptor file which contains the simulation setup, relative to the resolving path.
* -t: Path to generation target directory. The generated classes will be saved here.

In the following you can see a typical evocation of the CLI:
```
C:\Users\...\montisim-cli\target>java
    -jar montisim-cli-0.0.1-SNAPSHOT-jar-with-dependencies.jar
    -f example.composition
    -p C:\Users\...\montisim-cli\src\test\resources
    -t C:\Users\...\montisim-cli\target\generated-sources
```

After calling the CLI the `ConcreteExecution` and `StartExecution` classes are generated into directory given by -t.

The CLI is useful for quick experiments with the co-simulation framework. 
For scenarios which are likely to be developed over a longer period the [Maven Plugin](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/maven-cosimulation-gen-plugin) is highly recommended.
